import Discord from 'discord.js';
const client = new Discord.Client();
import dotenv from 'dotenv';
import axios from 'axios';
import fs from 'fs';
import { PrismaClient } from '.prisma/client';
const prisma = new PrismaClient();

dotenv.config();

import { getDotaLastMatch } from './dota-api';

client
  .login(process.env.DISCORD_TOKEN)
  .then(() => console.log('**** app is running ****'));

client.on('message', (message) => {
  if (message.content == `!${process.env.BOT_NAME}`) {
    message.channel.send('Aav ni irlee huuhduudee!!!');
  }
});

client.on('message', (message) => {
  // console.log(message);
  if (message.content == `!roll`) {
    let num = Math.floor(Math.random() * 100) + 1;
    let user = message.author.username;
    const kekw = message.guild.emojis.cache.find(({ name }) => name === 'kekw');
    // console.log({  });
    message.channel.send(
      `🎲🎲 ${user} rolled ${num} 🎲🎲 ${num < 10 ? kekw : ''}`
    );
  }
});

client.on('message', async (message) => {
  // Voice only works in guilds, if the message does not come from a guild,
  // we ignore it
  if (!message.guild) return;

  if (message.content.startsWith(`?`)) {
    let query = message.content
      .split(' ')
      .filter((value) => value != `?`)
      .join(' ');

    console.log(query);

    if (!query) return;

    if (message.member.voice.channel) {
      const connection = await message.member.voice.channel.join();
      axios
        .get(process.env.VOICE_API, {
          params: {
            voice: 3,
            text: query,
          },
          responseType: 'stream',
        })
        .then((res) => {
          console.log('downloaded successfully');
          res.data
            .pipe(fs.createWriteStream('./speech.mp3'))
            .on('finish', () => {
              console.log('playing voice');
              const dispatcher = connection.play('./speech.mp3', {
                volume: 0.5,
              });
            });
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      message.reply('voice channeldaaa or!');
    }
  }
});

client.on('message', async (message) => {
  if (!message.guild) return;

  if (message.content === '!invite-aduuch') {
    message.channel.send(
      `https://discord.com/oauth2/authorize?client_id=712302711680401409&scope=bot`
    );
  }
});

// dota api
client.on('message', async (message) => {
  if (!message.guild) return;

  if (message.content.startsWith(`!lastmatch`)) {
    // redis check if user exists
    // getDotaLastMatch(message, query, Discord);
  }
});

client.on('message', async (message) => {
  if (!message.guild) return;

  if (message.content.startsWith(`!lastmatch`)) {
    let query = message.content
      .split(' ')
      .filter((value) => value != `!lastmatch`)
      .join(' ');

    const link = await prisma.dotaLink.findUnique({
      where: {
        discordId: message.author.id,
      },
    });

    if (link) {
      getDotaLastMatch(message, link.steamId);
    } else {
      message.channel.send(`${message.author.username} no steamid linked.`);
    }
  }
});

client.on('message', async (message) => {
  if (!message.guild) return;

  if (message.content.startsWith(`!link-dota`)) {
    let query = message.content
      .split(' ')
      .filter((value) => value != `!link-dota`)
      .join(' ');

    console.log(query);

    const link = await prisma.dotaLink.findUnique({
      where: {
        discordId: message.author.id,
      },
    });

    if (link) {
      message.channel.send(`${message.author.username} already linked.`);
    } else {
      const newLink = await prisma.dotaLink.create({
        data: {
          discordId: message.author.id,
          steamId: query,
        },
      });

      if (newLink)
        message.channel.send(
          new Discord.MessageEmbed()
            .setTitle(`${message.author.username} successfully linked steam id`)
            .setDescription('try ```!lastmatch```')
        );
    }
  }
});

client.on('message', async (message) => {
  if (!message.guild) return;

  if (message.content.startsWith(`!unlink-dota`)) {
    let query = message.content
      .split(' ')
      .filter((value) => value != `!unlink-dota`)
      .join(' ');

    console.log(query);

    const deletedLink = await prisma.dotaLink
      .delete({
        where: {
          discordId: message.author.id,
        },
      })
      .catch(() => {
        message.channel.send(`${message.author.username} no steamid linked.`);
      });

    if (deletedLink) {
      message.channel.send(`${message.author.username} removed link.`);
    }
  }
});
